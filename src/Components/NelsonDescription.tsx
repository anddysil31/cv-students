import React from 'react';
import { Badge, Descriptions } from 'antd';

const Description: React.FC = () => (
    <Descriptions title="User Info" bordered>
        <Descriptions.Item label="First Name">Nelson</Descriptions.Item>
        <Descriptions.Item label="Second Name">David</Descriptions.Item>
        <Descriptions.Item label="Last Name">Cárdenas</Descriptions.Item>
        <Descriptions.Item label="Second Surname">Peñaranda</Descriptions.Item>
        <Descriptions.Item label="Age" span={2}>
            20
        </Descriptions.Item>
        <Descriptions.Item label="Status" span={3}>
            <Badge status="processing" text="Active" />
        </Descriptions.Item>
        <Descriptions.Item label="Salary">$800.00</Descriptions.Item>
        <Descriptions.Item label="Pc">Lenovo Ideapad Gaming 3</Descriptions.Item>
        <Descriptions.Item label="Schedule">8:30 - 16:00</Descriptions.Item>
        <Descriptions.Item label="Skills">
            Postgresql
            <br />
            kotlin
            <br />
            Networks
            <br />
            Electronics
            <br />
            .....
            <br />
            .....
            <br />
        </Descriptions.Item>
    </Descriptions>
);

export default Description;