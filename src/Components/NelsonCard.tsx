import React from 'react';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import { Avatar, Card } from 'antd';

const { Meta } = Card;

const NelsonCard: React.FC = () => (
    <Card
        style={{ width: 300 }}
        cover={
            <img
                alt="example"
                src="https://studiofutbol.com.ec/wp-content/uploads/2023/03/cr7.jpg"
            />
        }
        actions={[
            <SettingOutlined key="setting" />,
            <EditOutlined key="edit" />,
            <EllipsisOutlined key="ellipsis" />,
        ]}
    >
        <Meta
            avatar={<Avatar src="https://cdn4.iconfinder.com/data/icons/diversity-v2-0-volume-03/64/footballers-cristiano-ronaldo-512.png" />}
            title="Nelson Cárdenas"
            description="Student"
        />
    </Card>
);

export default NelsonCard;